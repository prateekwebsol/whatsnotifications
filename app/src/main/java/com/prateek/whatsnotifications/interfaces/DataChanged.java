package com.prateek.whatsnotifications.interfaces;

public interface DataChanged {
    void dataUpdated();
}
