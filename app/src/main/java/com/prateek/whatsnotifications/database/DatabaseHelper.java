package com.prateek.whatsnotifications.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(@Nullable Context context) {
        super(context, DatabaseConstants.DB_NAME, null, DatabaseConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String create = "CREATE TABLE IF NOT EXISTS " + DatabaseConstants.TABLE_NAME
                + "("
                + DatabaseConstants.KEY_ID + " INTEGER PRIMARY KEY,"
                + DatabaseConstants.KEY_CONTENT + " TEXT,"
                + DatabaseConstants.KEY_TITLE + " TEXT,"
                + DatabaseConstants.KEY_LAST_MODIFIED + "  INTEGER"
                + ")";
        Log.d("DB", create);
        sqLiteDatabase.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
