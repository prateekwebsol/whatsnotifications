package com.prateek.whatsnotifications.database;

public class DatabaseConstants {
    public final static String DB_NAME = "messages_db";
    public final static int DB_VERSION = 1;
    public final static String KEY_ID = "id";
    public final static String KEY_TITLE = "title";
    public final static String KEY_CONTENT = "content";
    public static final String KEY_LAST_MODIFIED = "modified_date";
    public static final String TABLE_NAME = "messages";
}
