package com.prateek.whatsnotifications.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.prateek.whatsnotifications.interfaces.DataChanged;
import com.prateek.whatsnotifications.models.Message;

import java.util.ArrayList;
import java.util.List;

public class MessagesDao {
    private Context context;
    private DatabaseHelper databaseHelper;
    private static MessagesDao messagesDao;
    private static DataChanged dataChanged;

    public static void bindListener(DataChanged listener){ dataChanged = listener; };


    private MessagesDao(Context context) {
        this.context = context;
        databaseHelper = new DatabaseHelper(context);
    }

    public static MessagesDao getInstance(Context context) {
        if (messagesDao == null) {
            messagesDao = new MessagesDao(context);
        }
        return messagesDao;
    }

    public void addMessage(Message model) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.KEY_TITLE, model.getTitle());
        values.put(DatabaseConstants.KEY_CONTENT, model.getContent());
        values.put(DatabaseConstants.KEY_LAST_MODIFIED, System.currentTimeMillis()/1000);
        database.insert(DatabaseConstants.TABLE_NAME, null, values);
        dataChanged.dataUpdated();
    }

    public List<Message> getAllMessages() {
        List<Message> list = new ArrayList<>();
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        String query = "SELECT * FROM " + DatabaseConstants.TABLE_NAME + " ORDER BY " + DatabaseConstants.KEY_LAST_MODIFIED + " DESC";
        Log.i("DB_QUERY", query);
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                @SuppressLint("Range") Message message = new Message(
                        cursor.getInt(cursor.getColumnIndex(DatabaseConstants.KEY_ID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseConstants.KEY_TITLE)),
                        cursor.getString(cursor.getColumnIndex(DatabaseConstants.KEY_CONTENT)),
                        cursor.getInt(cursor.getColumnIndex(DatabaseConstants.KEY_LAST_MODIFIED))
                );
                list.add(message);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return list;
    }


    public void deleteMessage(int id) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        database.delete(DatabaseConstants.TABLE_NAME, DatabaseConstants.KEY_ID + "=?", new String[]{String.valueOf(id)});
        dataChanged.dataUpdated();
    }
}
