package com.prateek.whatsnotifications.notificationHelper;

import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import androidx.core.app.NotificationCompat;

import com.prateek.whatsnotifications.database.MessagesDao;
import com.prateek.whatsnotifications.models.Message;

public class NotificationListener extends NotificationListenerService {
    static final String PACKAGE = "com.whatsapp";



    @Override
    public void onListenerConnected() {
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        if(sbn!=null){
            if(sbn.getPackageName().equals(PACKAGE)){
                Bundle bundle=sbn.getNotification().extras;
                String sender = bundle.getString(NotificationCompat.EXTRA_TITLE);
                String content = bundle.getString(NotificationCompat.EXTRA_TEXT);

                if(sender.equals("WhatsApp")){
                    return;
                }
                MessagesDao.getInstance(getApplicationContext()).addMessage(new Message(sender, content, (int) System.currentTimeMillis()));
            }
        }

    }
}
