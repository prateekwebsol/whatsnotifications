package com.prateek.whatsnotifications.ui.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prateek.whatsnotifications.R;
import com.prateek.whatsnotifications.database.MessagesDao;
import com.prateek.whatsnotifications.models.Message;


import java.util.ArrayList;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.viewHolder>{
    private Context context;
    private ArrayList<Message> list;

    public MessageAdapter(Context context, ArrayList<Message> list) {
        this.context = context;
        this.list = list;
    }

    public MessageAdapter() {
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.single_message, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.viewHolder holder, int position) {
        final Message thisMessage = list.get(position);
        holder.title.setText(thisMessage.getTitle());
        holder.content.setText(thisMessage.getContent());
        holder.root.setOnLongClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Are You Sure?"+thisMessage.getDateTime())
                    .setMessage("Do you want to delete the selected Message Permanently?")
                    .setPositiveButton("Yes", (dialogInterface, i) -> MessagesDao.getInstance(context.getApplicationContext()).deleteMessage(thisMessage.getId()))
                    .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class viewHolder extends RecyclerView.ViewHolder{
        TextView title, content, time;
        View root;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title_single);
            content=itemView.findViewById(R.id.content_single);
            root=itemView.findViewById(R.id.root);
        }
    }
}
