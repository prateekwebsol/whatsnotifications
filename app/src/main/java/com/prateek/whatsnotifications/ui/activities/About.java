package com.prateek.whatsnotifications.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.prateek.whatsnotifications.R;

public class About extends BaseActivity {
    private TextView contact;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        contact=findViewById(R.id.contact);

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/html");
                i.putExtra(Intent.EXTRA_EMAIL, "prateekwebsol@gmail.com");
                i.putExtra(Intent.EXTRA_SUBJECT, "Hi, I am Contacting you from your WhatsNotifications Application");
                startActivity(Intent.createChooser(i, "Choose the App By Which You Want to Send Email"));

            }
        });

    }
}
