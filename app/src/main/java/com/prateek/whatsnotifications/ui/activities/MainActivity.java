package com.prateek.whatsnotifications.ui.activities;


import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.prateek.whatsnotifications.R;
import com.prateek.whatsnotifications.database.MessagesDao;
import com.prateek.whatsnotifications.models.Message;
import com.prateek.whatsnotifications.notificationHelper.NotificationListener;
import com.prateek.whatsnotifications.ui.adapters.MessageAdapter;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {
    TextView empty;
    RecyclerView recyclerView;
    ImageView refresh;
    ArrayList<Message> list = new ArrayList<>();
    MessageAdapter messageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        empty=findViewById(R.id.empty);
        recyclerView=findViewById(R.id.recyclerView);
        refresh=findViewById(R.id.refresh);

        if(!isPermissionGranted()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Attention Please!")
                    .setMessage("The App Need Your Permission to read the Notificaions and Store them in Your Local Device. Please allow the App to Read Notifications so that you can enjoy the Application Smoothly.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
                        }
                    }).setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            })
                    .show();
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        messageAdapter = new MessageAdapter(this, list);
        recyclerView.setAdapter(messageAdapter);

        refreshData();

        refresh.setOnClickListener(view -> refreshData());
    }

    private boolean isPermissionGranted() {
        ComponentName cn = new ComponentName(this, NotificationListener.class);
        String flat = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        final boolean enabled = flat != null && flat.contains(cn.flattenToString());
        return enabled;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MessagesDao.bindListener(() -> refreshData());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void refreshData() {
        list.clear();
        list.addAll(MessagesDao.getInstance(getApplicationContext()).getAllMessages());
        if(list.size()==0){ empty.setVisibility(View.VISIBLE); }
        else empty.setVisibility(View.GONE);
        messageAdapter.notifyDataSetChanged();
    }
}